import React, { useState } from "react";

export const CounterApp = () => {
  const [counter, setCounter] = useState({
    counter1: 10,
    conunter2: 20,
  });

  const { counter1, conunter2 } = counter;

  const counterAdd = () => {
    setCounter({
      ...counter,
      counter1: counter1 + 1,
    });
  };
  const counterDelete = () => {
    if (counter1 > 0) {
      setCounter({
        ...counter,
        counter1: counter1 - 1,
      });
    }
  };
  return (
    <div>
      <>
        <h1>counter One {counter1}</h1>
        <h1>counter Two {conunter2}</h1>

        <hr />

        <button onClick={counterAdd} className="btn btn-primary mr-5">
          +1
        </button>
        <button onClick={counterDelete} className="btn btn-primary">
          -1
        </button>
      </>
    </div>
  );
};
