import React from "react";
import { useCounter } from "../../hooks/useCounter";

export const CounterCustomHook = () => {
  const { state, increment, decrement } = useCounter(100);

  return (
    <>
      <h1>Custom Hook conter {state}</h1>

      <button onClick={increment} className="btn btn-primary">
        +1
      </button>
      <button onClick={decrement} className="btn btn-success">
        -1
      </button>
    </>
  );
};
