import React, { useEffect } from "react";

export const Message = () => {
  useEffect(() => {
    //creacion del evento de movimiento del mouse
    const eventMouse = () => {
      console.log("Message realizados");
    };

    document.addEventListener("mousemove", eventMouse);

    //"unMounth" desmonta el evento listener para evitar la duplicacion
    return () => {
      document.removeEventListener("mousemove", eventMouse);
    };
  }, []);

  return <h1>Practica del unMount</h1>;
};
