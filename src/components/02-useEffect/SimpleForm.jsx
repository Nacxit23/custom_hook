import React, { useState } from "react";
import { Message } from "./Message";

export const SimpleForm = () => {
  const [form, setForm] = useState({
    name: "",
  });
  const getData = ({ target }) => {
    setForm({
      ...form,
      [target.name]: target.value,
    });
  };
  return (
    <>
      <h1>useState</h1>
      <form action="">
        <input type="text" name="name" onChange={getData} />
      </form>

      {form.name === "123" && <Message />}
    </>
  );
};
