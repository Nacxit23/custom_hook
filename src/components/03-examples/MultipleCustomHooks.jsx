import React from "react";
import { useCounter } from "../../hooks/useCounter";
// import { useAxios } from "../../hooks/useAxios";
import { useFetch } from "../../hooks/useFetch";

export const MultipleCustomHooks = () => {
  const { state, increment } = useCounter(1);

  // Con fetch
  const { loading, data } = useFetch(
    `https://www.breakingbadapi.com/api/quotes/${state}`
  );

  //Con axios
  // const axios = useAxios("https://www.breakingbadapi.com/api/quotes/1");

  const { author, quote } = !!data && data[0];

  return (
    <div>
      <h1>CustomHook</h1>
      <hr />
      {loading ? (
        <div className="alert alert-info text-center">Loading...</div>
      ) : (
        <blockquote className="blockquote text-right">
          <p className="mb-0">{quote}</p>
          <footer className="blockquote-footer">{author}</footer>
        </blockquote>
      )}

      <button className="btn btn-primary" onClick={increment}>
        Siguiente quote
      </button>
    </div>
  );
};
