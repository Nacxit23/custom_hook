import React, { useRef } from "react";

export const FocusScreen = () => {
  const ref = useRef(null);

  const hadleClick = () => {
    ref.current.select();
  };

  return (
    <>
      <h1>Focus Screen</h1>
      <hr />
      <input ref={ref} className="form-control" placeholder="Name" />
      <button onClick={hadleClick} className="btn btn-outline-primary mt-5">
        Focus
      </button>
    </>
  );
};
