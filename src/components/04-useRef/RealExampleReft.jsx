import React, { useState } from "react";
import { MultipleCustomHooks } from "../03-examples/MultipleCustomHooks";

export const RealExampleReft = () => {
  const [show, setShow] = useState(false);

  const showHide = () => {
    setShow(!show);
  };

  return (
    <div>
      <h1>Real example Relf</h1>
      <hr />
      {show && <MultipleCustomHooks />}

      <button onClick={showHide} className="btn btn-primary mt-5">
        Show/hide
      </button>
    </div>
  );
};
