import React, { useRef, useState } from "react";

export const UseRefP1 = () => {
  const divColor = useRef(null);

  const [celcio, setCelcio] = useState({
    getCelcio: 0,
  });
  const fahrenheit = useRef(null);

  const celcioF = () => {
    const fahrenheitValue = fahrenheit.current.value;

    const convertCelcio = (fahrenheitValue - 32) / 1.8;

    setCelcio({
      getCelcio: convertCelcio,
    });
  };

  const cambiarColor = () => {
    divColor.current.style.backgroundColor = "yellow";
  };

  return (
    <div>
      <div className="col-lx-12 mb-5">
        <div style={{ height: "100px", width: "150px" }} ref={divColor}>
          Color: {divColor.current}
        </div>
        <input type="number" placeholder="Fahrenheit" ref={fahrenheit} />
        <p className="DOLAR">{celcio.getCelcio}</p>
        <button className="btn btn-primary" onClick={cambiarColor}>
          Cambiar color
        </button>
        <button className="btn btn-danger" onClick={celcioF}>
          Fahrenheit/Celcio
        </button>
      </div>
    </div>
  );
};
