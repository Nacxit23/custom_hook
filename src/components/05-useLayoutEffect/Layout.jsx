import React from "react";
import { useCounter } from "../../hooks/useCounter";
// import { useAxios } from "../../hooks/useAxios";
import { useFetch } from "../../hooks/useFetch";

export const Layout = () => {
  const { state, increment } = useCounter(1);

  // Con fetch
  const { data } = useFetch(
    `https://www.breakingbadapi.com/api/quotes/${state}`
  );

  //Con axios
  // const axios = useAxios("https://www.breakingbadapi.com/api/quotes/1");

  const { quote } = !!data && data[0];

  return (
    <div>
      <h1>Layout Effect</h1>
      <hr />

      <blockquote className="blockquote text-right">
        <p className="mb-0">{quote}</p>
        <footer className="blockquote-footer"></footer>
      </blockquote>

      <button className="btn btn-primary" onClick={increment}>
        Siguiente quote
      </button>
    </div>
  );
};
