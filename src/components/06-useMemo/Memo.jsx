import React, { useState, useMemo } from "react";
import { useCounter } from "../../hooks/useCounter";

export const Memo = () => {
  const { state, increment } = useCounter(5000);
  const [show, setShow] = useState(true);

  const showHide = () => {
    setShow(!show);
  };

  const hardProcess = (interacion) => {
    for (let i = 0; i < interacion; i++) {
      console.log("sdsdsds");
    }

    return `${interacion} interaciones r`;
  };

  //Memorizando el resultado de la peticion del
  //metado dificil permitira a no reperir los resultados mostrados
  const memoHard = useMemo(() => hardProcess(state), [state]);

  return (
    <>
      <h1>MEMO HOOK</h1>
      <h4>Memorize counter {state}</h4>

      <p>{memoHard}</p>
      <button onClick={increment}>Aumentar</button>
      <button onClick={showHide}>ShowHide</button>
    </>
  );
};
