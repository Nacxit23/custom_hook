import React from "react";
import { useCounter } from "../../hooks/useCounter";
import { Small } from "./Small";

export const Memorize = () => {
  const { state, increment } = useCounter(1);

  return (
    <>
      <h1>
        Memorize counter <Small value={state} />
      </h1>
      <button onClick={increment}>Aumentar</button>
    </>
  );
};
