import React from "react";
import ProtoTypes from "prop-types";

export const Small = ({ value }) => {
  return <small>{value}</small>;
};

Function.protoTypes = {
  value: ProtoTypes.number,
};
