import React, { useEffect, useReducer } from "react";
import { useForm } from "../../hooks/useForm";
import { tareasReduce } from "./tareasReduce";

const init = () => {
  return JSON.parse(localStorage.getItem("tareas")) || [];
};

export const UseReduce = () => {
  const [tareas, dispatch] = useReducer(tareasReduce, [], init);

  const [{ dataSubmitTodo }, hadledInputChange] = useForm({
    dataSubmitTodo: "",
  });

  useEffect(() => {
    localStorage.setItem("tareas", JSON.stringify(tareas));
  }, [tareas]);

  const hadledSubmit = (e) => {
    e.preventDefault();

    const newData = {
      id: new Date().getTime(),
      todo: dataSubmitTodo,
      done: false,
    };

    const action = {
      type: "add",
      payload: newData,
    };
    dispatch(action);
  };

  const hadlesDelte = (tareasID) => {
    console.log(tareasID);
    const deleteData = {
      type: "delete",
      payload: tareasID,
    };

    dispatch(deleteData);
  };
  return (
    <div className="conteiner">
      <h1>UseReduce ({tareas.length})</h1>
      <hr />
      <div className="row">
        <div className="col-3">
          <ul>
            {tareas.map((n, i) => (
              <ul key={i}>
                <li>
                  {n.id}
                  <button
                    onClick={() => hadlesDelte(n.id)}
                    className="btn btn-danger"
                  >
                    Borrar
                  </button>
                </li>
                <li>{n.todo}</li>
              </ul>
            ))}
          </ul>
        </div>
        <div className="col-6">
          <h5>Agregar tarea</h5>
          <br />
          <form onSubmit={hadledSubmit}>
            <input
              type="text"
              name="dataSubmitTodo"
              placeholder="agregar todo"
              value={dataSubmitTodo}
              onChange={hadledInputChange}
            />
            <button className="btn btn-success"> agregar</button>
          </form>
        </div>
      </div>
    </div>
  );
};
