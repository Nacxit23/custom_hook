const tareas = [
  {
    id: 1,
    todo: "comprar pan",
    done: false,
  },
];

const tareasReduces = (state = tareas, action) => {
  if (action?.type === "agregar") {
    return [...state, action.payLoad];
  }

  return state;
};

let todos = tareasReduces();

const tarea2 = {
  id: 2,
  todo: "comprar manzana",
  done: false,
};

const newTareaAction = {
  type: "agregar",
  payLoad: tarea2,
};

todos = tareasReduces(tareas, newTareaAction);

console.log(todos);
