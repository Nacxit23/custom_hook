import React, { useContext } from "react";
import { UserContext } from "./UserContext";

export const HomeScreen = () => {
  const user = useContext(UserContext);

  return (
    <div>
      <h1>Home Screen</h1>
      {JSON.stringify(user, null, 3)}
      <hr />
    </div>
  );
};
