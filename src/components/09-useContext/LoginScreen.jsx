import React, { useContext } from "react";
import { UserContext } from "./UserContext";

export const LoginScreen = () => {
  const { setUser } = useContext(UserContext);

  const objec = {
    name: "Snoopy",
    raza: "perro",
  };

  return (
    <div>
      <h1>login Screen</h1>
      <hr />
      <button className="btn btn-primary" onClick={() => setUser(objec)}>
        Login
      </button>
    </div>
  );
};
