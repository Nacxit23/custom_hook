import React, { useState } from "react";
import { Routers } from "./Routers";
import { UserContext } from "./UserContext";

export const MainApp = () => {
  const [user, setUser] = useState({});

  return (
    //EL USERCONTEXT ES UN HIGHORDERCOMPONENT
    //Provider permite proveer informacion a todos los componentes hijos del mismo
    //value es el que se encarga en capturar la informacion que se le envia
    <UserContext.Provider value={{ user, setUser }}>
      <Routers />
    </UserContext.Provider>
  );
};
