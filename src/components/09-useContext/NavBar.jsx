import React from "react";
import { NavLink } from "react-router-dom";

export const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <NavLink
            className="nav-link nav-item"
            activeClassName="active"
            exact
            to="/"
          >
            Home
          </NavLink>
          <NavLink
            className="nav-link nav-item"
            activeClassName="active"
            exact
            to="/login"
          >
            Login
          </NavLink>
          <NavLink
            className="nav-link nav-item"
            activeClassName="active"
            exact
            to="/about"
          >
            About
          </NavLink>
        </div>
      </div>
    </nav>
  );
};
