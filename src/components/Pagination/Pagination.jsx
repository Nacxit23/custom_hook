import React, { useRef, useState } from "react";
import { NavLink } from "react-router-dom";
const Pagination = ({ totalData, postsPerPage, paginate }) => {
  const pageNumbers = [];

  //Calcula la cantidad de paginaciones con respecto a los registro mostrados o adquirido
  for (let i = 1; i <= Math.ceil(totalData / postsPerPage); i++) {
    pageNumbers.push(i);
  }
  //permite adquirir la paginacion actual del usuario para utilizarno en after and bejore page
  const [actualNumbert, setActualNumbert] = useState();
  const previusNextButton = useRef(null);

  const actionButton = (event) => {
    event.preventDefault();

    if (actualNumbert !== 1) {
      const afterNumber = actualNumbert - 1;
      paginate(afterNumber);
      setActualNumbert(afterNumber);
    } else {
      previusNextButton.current.style.color = "red";
    }
  };

  return (
    <nav aria-label="...">
      <ul className="pagination">
        <li className="page-item">
          <NavLink
            ref={previusNextButton}
            onClick={actionButton}
            className="page-link"
            to="!#"
            activeClassName="active"
            exact
          >
            Previus
          </NavLink>
        </li>
        {pageNumbers.map((number) => (
          <li key={number} className="page-item">
            <NavLink
              onClick={(event) => {
                event.preventDefault();
                previusNextButton.current.style.color = "blue";
                setActualNumbert(number);
                paginate(number);
              }}
              className="page-link"
              to="!#"
              activeClassName="active"
              exact
            >
              {number}
            </NavLink>
          </li>
        ))}
        <li className="page-item">
          <NavLink
            ref={previusNextButton}
            onClick={(event) => {
              event.preventDefault();

              const beforeNumber = actualNumbert + 1;
              paginate(beforeNumber);
              setActualNumbert(beforeNumber);
            }}
            className="page-link"
            to="!#"
            activeClassName="active"
            exact
          >
            next
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};
export default Pagination;
