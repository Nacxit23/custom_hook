import React, { useReducer } from "react";
import { reduce } from "./Reduce";

export const HookRPractice = () => {
  const initialState = {
    count: 0,
  };

  const [state, dispatch] = useReducer(reduce, initialState);

  return (
    <div>
      <h1>count {state.count}</h1>
      <button
        onClick={() =>
          dispatch({
            style: "increment",
          })
        }
      >
        +1
      </button>
      <button
        onClick={() =>
          dispatch({
            style: "decrement",
          })
        }
      >
        -1
      </button>
    </div>
  );
};
