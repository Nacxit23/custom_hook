import Axios from "axios";
import { useState, useEffect, useRef } from "react";

export const useAxios = (url, currentPager, numberPage) => {
  //useState que se encarga de almacenar los datos recolectados de la api
  const [data, setdata] = useState([]);
  //useState que se encargar de enviarMensaje acerca del error recolectado
  const [error, setError] = useState(null);

  //operaciones correspondiente para la paginacion de resultados
  const [currentPage, setCurrentPage] = useState(currentPager);
  const [postsPerPage] = useState(numberPage);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);

  const isMountan = useRef(true);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  useEffect(() => {
    return () => {
      isMountan.current = false;
    };
  }, []);

  //useEffect get con una funcion asincrona (async)
  useEffect(() => {
    const apiData = async () => {
      setError(null);
      try {
        const json = await Axios.get(url);

        if (isMountan.current) {
          setdata(json.data);
        }
      } catch (error) {
        setError(error.response);
      }
    };
    apiData();
  }, [url]);

  let finalDataGet = {};
  //permite monstrar el error en los resultados como tambien los datos obtenidos
  if (error) {
    finalDataGet = error;
  } else {
    finalDataGet = currentPosts;
  }
  //funcion post
  return { finalDataGet, postsPerPage, paginate, data };
};


//Uso
/* 1) Se le tiene agregar la api la cual quiere ingresar el hook
   2) Los saltos de paginaciones que se decea realizar
   3) Cantidad de item que quieres mostrar por paginacion
*/ 


// export const Home = () => {
//   const {finalDataGet, postsPerPage, paginate, data}= useAxios(
//     "https://jsonplaceholder.typicode.com/todos",
//     1,
//     14
//   );
