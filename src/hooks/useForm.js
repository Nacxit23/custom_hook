import { useState } from "react";

// este hook resumen las funciones de uso para el ingresado de dato a travez de un formulario

export const useForm = (initialState = {}) => {
  // se utiliza useState para capturar los datos ingresado a travez de cada imput
  const [values, setValues] = useState(initialState);

  const reset = () => {
    setValues(initialState);
  };

  const handleInputChange = ({ target }) => {
    setValues({
      ...values,
      [target.name]: target.value,
    });
  };

  return [values, handleInputChange, reset];
};

// utilizacion
// 1) tiene que inicializar los datos primeros puede ser de cualquier tipo,
// 2) habdleInputchange es el llamado del evento se suele utilizar en onChange
// 3) formVlue permite acceder a los datos adquirido a travez del evento

// const [formValues, handleInputChange] = useForm({
//   seachText: q,
// });
